#
# This file is autogenerated by pip-compile with python 3.10
# To update, run:
#
#    pip-compile --output-file=requirements/base.txt requirements/base.in
#
amqp==5.1.1
    # via kombu
argon2-cffi==21.3.0
    # via -r requirements/base.in
argon2-cffi-bindings==21.2.0
    # via argon2-cffi
asgiref==3.5.2
    # via django
async-timeout==4.0.2
    # via redis
billiard==3.6.4.0
    # via celery
boto3==1.24.79
    # via ssm-parameter-store
botocore==1.27.79
    # via
    #   boto3
    #   s3transfer
brotli==1.0.9
    # via whitenoise
celery[redis]==5.2.7
    # via
    #   -r requirements/base.in
    #   celery-singleton
    #   django-celery-beat
    #   django-celery-results
celery-singleton==0.3.1
    # via -r requirements/base.in
certifi==2022.9.14
    # via
    #   requests
    #   sentry-sdk
cffi==1.15.1
    # via argon2-cffi-bindings
charset-normalizer==2.1.1
    # via requests
click==8.1.3
    # via
    #   celery
    #   click-didyoumean
    #   click-plugins
    #   click-repl
    #   rq
click-didyoumean==0.3.0
    # via celery
click-plugins==1.1.1
    # via celery
click-repl==0.2.0
    # via celery
crispy-forms-gds==0.2.4
    # via -r requirements/base.in
croniter==1.3.7
    # via rq-scheduler
defusedxml==0.7.1
    # via odfpy
deprecated==1.2.13
    # via redis
diff-match-patch==20200713
    # via django-import-export
django==3.2.15
    # via
    #   -r requirements/base.in
    #   crispy-forms-gds
    #   django-anymail
    #   django-appconf
    #   django-celery-beat
    #   django-import-export
    #   django-rq
django-anymail[mailgun]==8.6
    # via -r requirements/base.in
django-appconf==1.0.5
    # via django-compressor
django-celery-beat==2.4.0
    # via -r requirements/base.in
django-celery-results==2.4.0
    # via -r requirements/base.in
django-compressor==4.1
    # via -r requirements/base.in
django-crispy-forms==1.14.0
    # via crispy-forms-gds
django-environ==0.9.0
    # via -r requirements/base.in
django-import-export==2.9.0
    # via -r requirements/base.in
django-rq==2.5.1
    # via -r requirements/base.in
django-sass-processor==1.2.1
    # via -r requirements/base.in
django-timezone-field==5.0
    # via django-celery-beat
django-waffle==3.0.0
    # via -r requirements/base.in
et-xmlfile==1.1.0
    # via openpyxl
gunicorn==20.1.0
    # via -r requirements/base.in
idna==3.4
    # via requests
jmespath==1.0.1
    # via
    #   boto3
    #   botocore
kombu==5.2.4
    # via celery
libsass==0.21.0
    # via -r requirements/base.in
markuppy==1.14
    # via tablib
oauthlib==3.2.2
    # via
    #   -r requirements/base.in
    #   requests-oauthlib
odfpy==1.4.1
    # via tablib
openpyxl==3.0.10
    # via tablib
packaging==21.3
    # via redis
prompt-toolkit==3.0.32
    # via click-repl
psycopg2-binary==2.9.3
    # via -r requirements/base.in
pycparser==2.21
    # via cffi
pyparsing==3.0.9
    # via packaging
python-crontab==2.6.0
    # via django-celery-beat
python-dateutil==2.8.2
    # via
    #   -r requirements/base.in
    #   botocore
    #   croniter
    #   python-crontab
    #   rq-scheduler
pytz==2022.2.1
    # via
    #   celery
    #   django
    #   django-timezone-field
pyyaml==6.0
    # via tablib
rcssmin==1.1.0
    # via django-compressor
redis==4.3.4
    # via
    #   celery
    #   celery-singleton
    #   django-rq
    #   rq
requests==2.28.1
    # via
    #   -r requirements/base.in
    #   django-anymail
    #   requests-oauthlib
requests-oauthlib==1.3.1
    # via -r requirements/base.in
rjsmin==1.2.0
    # via django-compressor
rq==1.11.0
    # via
    #   -r requirements/base.in
    #   django-rq
    #   rq-scheduler
rq-scheduler==0.11.0
    # via -r requirements/base.in
s3transfer==0.6.0
    # via boto3
sentry-sdk==1.9.8
    # via -r requirements/base.in
six==1.16.0
    # via
    #   click-repl
    #   libsass
    #   python-dateutil
sqlparse==0.4.2
    # via django
ssm-parameter-store==19.11.0
    # via -r requirements/base.in
tablib[html,ods,xls,xlsx,yaml]==3.2.1
    # via django-import-export
tzdata==2022.6
    # via django-celery-beat
urllib3==1.26.12
    # via
    #   botocore
    #   requests
    #   sentry-sdk
vine==5.0.0
    # via
    #   amqp
    #   celery
    #   kombu
wcwidth==0.2.5
    # via prompt-toolkit
whitenoise[brotli]==6.2.0
    # via -r requirements/base.in
wrapt==1.14.1
    # via deprecated
xlrd==2.0.1
    # via tablib
xlwt==1.3.0
    # via tablib

# The following packages are considered to be unsafe in a requirements file:
# setuptools
